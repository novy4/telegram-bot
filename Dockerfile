FROM alpine:3.7

MAINTAINER Alex Novacovschi <alex.novacovschi@endava.com>

# Add the files
ADD icodrops.py /root/

RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    pip3 install telethon && \
    pip3 install tornado && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python:3 /usr/bin/python; fi && \
    rm -r /root/.cache


#VOLUME /web

#EXPOSE 8083
